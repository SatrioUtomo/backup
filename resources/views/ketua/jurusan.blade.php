@extends('ketua.template.v_template')
@section('title','JURUSAN')
@section('content')
    
<br>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" integrity="sha512-wJgJNTBBkLit7ymC6vvzM1EcSWeM9mmOu+1USHaRBbHkm6W9EgM0HY27+UtUaprntaYQJF75rc8gjxllKs5OIQ==" crossorigin="anonymous" />

{{-- Awal Alert --}}
    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> SELAMAT DATANG DI HALAMAN @yield('title')</h4>
        Pada halaman anda dapat melihat dan menambah data jurusan. . . 
    </div>
{{-- Akhir Aler --}}

{{-- AWAL PANITIA --}}
<div class="container-fluid">
    <div class="row">
      <div class="col-lg-9 margin-tb">
          <h3 class="pl-2" style="border-left: solid black 5px">&nbsp;List Data Jurusan</h3>
      </div>
  </div>
  <hr>

<div class="container" style="margin-left: -15px;">
    <a href="/ketua/jurusan/tambah" class="btn btn-warning">+ Tambah Jurusan</a>
 </div>
 <br>
 @if(Session::has('deleted'))
     <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{Session::get('deleted')}}
    </div>
 @endif

      <table class="table" id="table-pengajuan">
          <thead class="table" style="background-color: #18A558"  >
              <tr>
              <th style="color: white">ID JURUSAN</th>
              <th style="color: white">NAMA</th>
              <th style="color: white">PILIHAN</th>
          </tr>
      {{-- </thead>
      <tbody>
          @foreach($panitia as $p)
          <tr>
                  <td>{{$p->id_panitia}}</td>
                  <td>{{$p->id_rapat}}</td>
                  <td>{{$p->kp}}</td>
                  <td>{{$p->siacara}}</td>
                  <td>{{$p->sihumas}}</td>
                  <td>{{$p->siperlengkapan}}</td>
                  <td>{{$p->sikonsumsi}}</td>
                  <td>{{$p->sidokumentasi}}</td>
              
                <td> 
                    <a href="/ketua/jurusan/edit/{{ $p->id_panitia }}" class="btn btn-info btn-sm"><i class="fa fa-check"></i> Edit Data</a> &nbsp;
                    <a href="/ketua/jurusan/hapus/{{ $p->id_panitia }}" onclick="return confirm('Apakah anda yakin untuk menghapus?')"  class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus Data</a>
                </td>    
              </tr>
          @endforeach --}}
      </tbody>
  </table>
</div>
{{-- js --}}
{{-- @if(Session::has('deleted'))
    <script>
        toasts.success("{!! Session::get('deleted') !!}");
    </script>
@endif --}}
<script>
  
    $(document).ready(function() {
        $('#table-pengajuan').DataTable({
            "columnDefs": [{
                "orderable": false,
                "searchable": true,
                "targets": 1
            }],
            "aLengthMenu": [
                [5, 10, 25, -1],
                [5, 10, 25, "All"]
            ],
            "iDisplayLength": 5
        });
    });
    /* Because i didnt set placeholder values in forms.py they will be set here using vanilla Javascript
    //We start indexing at one because CSRF_token is considered and input field
     */
    
    //Query All input fields
    var select_fields = document.getElementsByTagName('select')
    
    var input_fields = document.getElementsByTagName('input')
    
    
    for (var field in select_fields) {
        select_fields[field].className += ' form-control'
    }
    for (var field in input_fields) {
        input_fields[field].className += ' form-control'
    }
    </script>
{{-- js --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js" integrity="sha512-zlWWyZq71UMApAjih4WkaRpikgY9Bz1oXIW5G0fED4vk14JjGlQ1UmkGM392jEULP8jbNMiwLWdM8Z87Hu88Fw==" crossorigin="anonymous"></script>

{{-- AKHIR PANITIA --}}
@endsection
