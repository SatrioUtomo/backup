@extends('ketua.template.v_template')

@section('content')

{{-- Header Form --}}
<div class="row">
    <div class="col-lg-9 margin-tb">
        <h3 class="pl-2" style="border-left: solid black 5px">&nbsp;Form Panitia Program Kerja</h3>
    </div>
</div>
<hr>
{{-- Header Form --}}

{{-- Awal Alert --}}
<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> silakan mengisi data panitia program kerja</h4>
    Pada form yang telah disediakan berikut. . . 
</div>
{{-- Akhir Aler --}}


<form action="/ketua/panitia/store" method="POST" enctype="multipart/form-data">

    @csrf

      
    <div class="content">
        <div class="row">
            
            <div class="col-sm-8">
           
            <div class="form-group">
                <label>ID USER</label>
                <select class="user2" name="id_user" class="form-control">
                    @forelse ($user as $item)
                       <option value="{{$item->id}}">{{$item->name}}</option>
                       @empty
                       <option value="0">data kosong</option>
                    @endforelse
                </select>
            </div>
            @if($errors->has('id_user'))
                <div class="text-danger">
                    {{ $errors->first('id_user')}}
                </div>
             @endif
             
             <div class="form-group">
                 <label>ID PROKER</label>
                 <select class="proker2" name="id_proker" class="form-control">
                    @forelse ($proker as $item)
                       <option value="{{$item->id}}">{{$item->nama_proker}}</option>
                       @empty
                       <option value="0">data kosong</option>
                    @endforelse
                </select>
                </div>
            @if($errors->has('id_proker'))
                    <div class="text-danger">
                        {{ $errors->first('id_proker')}}
                    </div>
            @endif

            <div class="form-group">
                <label>JABATAN</label>
                <input type="text" name="jabatan" class="form-control">
            </div>
            @if($errors->has('jabatan'))
                    <div class="text-danger">
                        {{ $errors->first('jabatan')}}
                    </div>
            @endif
            
           
        </div>
    </div>
    <div class="form-group">
        <button class="btn btn-success btn-sm"  style="margin-left: 8pt;" onclick="return confirm('Apakah data anda sudah benar?')"><i class="fa fa-plus"></i> &nbsp;TAMBAH</button>
        </div>
        
    </div>

</form>
@if(Session::has(''))
    <script>
        toasts.success("{!! Session::get('') !!}");
    </script>
@endif

<script>
    $(document).ready(function() {
        $(".user2").select2({
            width: '100%'
        });
        
    });
</script>

<script>
    $(document).ready(function() {
        $(".proker2").select2({
            width: '100%'
        });
        
    });
</script>
@endsection