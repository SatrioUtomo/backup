@extends('ketua.template.v_template')

@section('content')

{{-- Alert --}}
<div class="container">
    <h1><b>UPDATE ANGGOTA</b></h1><br>
    <div class="alert alert-success" role="alert">
        Silahkan anda mengupdate form. . . .
</div>
{{-- Alert --}}

    @foreach($anggota as $p)
    <form class="col-md-12" action="/ketua/anggota/edit" method="post">
    {{ csrf_field() }}
      <div class="hidden">
        <label for="id_anggota" class="form-label"><b></b></label>
        <input type="hidden" class="form-control" name="id_anggota" value="{{ $p->id_anggota }}" >
      </div> 
    
      <div class="row">
        <div class="form-group col-sm-6">
        
          <label for="nama_ang" class="form-label"><b>NAMA ANGGOTA</b></label>
          <input type="text" class="form-control" name="nama_ang" value="{{ $p->nama_ang }}" >
       
          <label for="jabatan" class="form-label"><b>JABATAN</b></label>
          <select class="form-control"  name="jabatan">
                    <option hidden value="{{ $p->jabatan }}"> {{ $p->jabatan }} </option>
                    <option value="KETUA"> KETUA
                    </option>
                    <option value="WAKIL KETUA"> WAKIL KETUA
                    </option>
                    <option value="SEKERTARIS"> SEKERTARIS
                    </option>
                    <option value="BENDAHARA"> BENDAHARA
                    </option>
                    <option value="ANGGOTA"> ANGGOTA
                    </option>
                </select>
          
          <label for="alamat" class="form-label"><b>ALAMAT</b></label>
          <input type="text" class="form-control" name="alamat" value="{{ $p->alamat }}" >
        
      
          <label for="semester" class="form-label"><b>SEMESTER</b></label>
          <select class="form-control"  name="semester">
                    <option hidden value="{{ $p->semester }}"> {{ $p->semester }} </option>
                    <option value="I"> I
                    </option>
                    <option value="II"> II
                    </option>
                    <option value="III"> III
                    </option>
                    <option value="IV"> IV
                    </option>
                    <option value="V"> V
                    </option>
                    <option value="VI"> VI
                    </option>
                    <option value="VI"> VII
                    </option>
                    <option value="VI"> VIII
                    </option>
                </select>


        </div>
      
        <div class="form-group col-sm-6">

          <label for="jurusan" class="form-label"><b>JURUSAN</b></label>
          <input type="text" class="form-control" name="jurusan" value="{{ $p->jurusan }}" >
           

          <label for="kelas" class="form-label"><b>KELAS</b></label>
          <select class="form-control"  name="kelas">
                    <option hidden value="{{ $p->kelas }}"> {{ $p->kelas }} </option>
                    <option value="1"> 1
                    </option>
                    <option value="2"> 2
                    </option>
                    <option value="3"> 3
                    </option>
                    <option value="4"> 4
                    </option>
                </select>

          <label for="no_hp" class="form-label"><b>NO HP</b></label>
          <input type="text" class="form-control" name="no_hp" value="{{ $p->no_hp }}" >

          <label for="email" class="form-label"><b>EMAIL</b></label>
          <input type="text" class="form-control" name="email" value="{{ $p->email }}" >

          <label for="status" class="form-label"><b>STATUS</b></label>
          <select class="form-control"  name="status">
                    <option hidden value="{{ $p->status }}"> {{ $p->status }} </option>
                    <option value="AKTIF"> AKTIF
                    </option>
                    <option value="NON-AKTIF"> NON-AKTIF
                    </option>
                </select>
        
      
     
    
      <button type="submit" class="btn btn-info"><i class="fa fa-edit"></i> &nbsp; UPDATE</button> &nbsp;
      <a href="{{ URL::previous() }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> &nbsp; KEMBALI</a>
    
    
    </div>
    </div>
    </form>
    @endforeach
    
    <script>
      $(document).ready(function() {
          $(".user").select2({
              width: '100%'
          });
          
      });
  </script>
@endsection