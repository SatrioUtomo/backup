@extends('ketua.template.v_template')

@section('content')

{{-- Alert --}}
<div class="container">
    <h1><b>UPDATE PENDAFTAR</b></h1><br>
    <div class="alert alert-success" role="alert">
        Silahkan anda mengupdate form. . . .
</div>
{{-- Alert --}}

    @foreach($daftar as $d)
    <form class="col-md-12" action="/ketua/daftar/edit" method="post">
    {{ csrf_field() }}
      <div class="hidden">
        <label for="id_daftar" class="form-label"><b></b></label>
        <input type="hidden" class="form-control" name="id_daftar" value="{{ $d->id_daftar }}" >
      </div> 
    
      <div class="row">
        <div class="form-group col-sm-6">
        
          <label for="nama_daf" class="form-label"><b>NAMA PENDAFTAR</b></label>
          <input type="text" class="form-control" name="nama_daf" value="{{ $d->nama_daf }}" >
    
        
          <label for="alamat" class="form-label"><b>ALAMAT</b></label>
          <input type="text" class="form-control" name="alamat" value="{{ $d->alamat }}" >
        
          <label for="kelas" class="form-label"><b>KELAS</b></label>
          <select class="form-control"  name="kelas">
                    <option hidden value="{{ $d->kelas }}"> {{ $d->kelas }} </option>
                    <option value="1"> 1
                    </option>
                    <option value="2"> 2
                    </option>
                    <option value="3"> 3
                    </option>
                    <option value="4"> 4
                    </option>
                </select>

          <label for="jurusan" class="form-label"><b>JURUSAN</b></label>
          <input type="text" class="form-control" name="jurusan" value="{{ $d->jurusan }}" >

        </div>
      
        <div class="form-group col-sm-6">
         
          <label for="motivasi" class="form-label"><b>MOTIVASI</b></label>
          <input type="text" class="form-control" name="motivasi" value="{{ $d->motivasi }}" >

          <label for="email" class="form-label"><b>EMAIL</b></label>
          <input type="text" class="form-control" name="email" value="{{ $d->email }}" >

          <label for="email" class="form-label"><b>STATUS</b></label>
          <select class="form-control"  name="status">
                    <option hidden value="{{ $d->status }}"> {{ $d->status }} </option>
                    <option value="Diterima"> Diterima
                    </option>
                    <option value="Tidak Diterima"> Tidak Diterima
                    </option>
                </select>
        

          <label for="file" class="form-label"><b>FILE</b></label>
          <form action="/action_page.php">
          <input type="file" class="form-control" name="file" value="{{ $d->file }}" >
          </form>
      
     
    
      <button type="submit" class="btn btn-info"><i class="fa fa-edit"></i> &nbsp; UPDATE</button> &nbsp;
      <a href="{{ URL::previous() }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> &nbsp; KEMBALI</a>
    
    
    </div>
    </div>
    </form>
    @endforeach
    
    <script>
      $(document).ready(function() {
          $(".user").select2({
              width: '100%'
          });
          
      });
  </script>
@endsection