@extends('ketua.template.v_template')

@section('content')

{{-- Header Form --}}
<div class="row">
    <div class="col-lg-9 margin-tb">
        <h3 class="pl-2" style="border-left: solid black 5px">&nbsp;Form Tambah Pendaftar</h3>
    </div>
</div>
<hr>
{{-- Header Form --}}

{{-- Awal Alert --}}
<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Silahkan mengisi data pendaftar</h4>
    Pada form yang telah disediakan berikut. . . 
</div>
{{-- Akhir Alert --}}

<form action="/ketua/daftar/store" method="POST" enctype="multipart/form-data">

    @csrf

    <div class="content">
        <div class="row">
            
            <div class="col-sm-6">
           
            <div class="form-group">
                <label>ID PENDAFTAR</label>
                <input type="text" name="id_daftar" placeholder="Masukkan ID Pendaftar" class="form-control">
            </div>
            @if($errors->has('id_daftar'))
                    <div class="text-danger">
                        {{ $errors->first('id_daftar')}}
                    </div>
            @endif

            <div class="form-group">
                <label>NAMA PENDAFTAR</label>
                <input type="text" name="nama_daf" placeholder="Masukkan Nama Pendaftar" class="form-control">
            </div>
            @if($errors->has('nama_daf'))
                    <div class="text-danger">
                        {{ $errors->first('nama_daf')}}
                    </div>
            @endif

            <div class="form-group">
                <label>ALAMAT</label>
                <input type="text" name="alamat" placeholder="Masukkan Alamat" class="form-control">
            </div>
            @if($errors->has('alamat'))
                    <div class="text-danger">
                        {{ $errors->first('alamat')}}
                    </div>
            @endif

            <div class="form-group">
                <label>KELAS</label>
                <select class="form-control" required name="kelas">
                <option disabled hidden selected> Pilih Kelas </option>
                    <option value="1"> 1
                    </option>
                    <option value="2"> 2
                    </option>
                    <option value="3"> 3
                    </option>
                </select>
            </div>
            @if($errors->has('kelas'))
                    <div class="text-danger">
                        {{ $errors->first('kelas')}}
                    </div>
            @endif

        </div>

            <div class="col-sm-6">
        <div class="form-group">
                <label>JURUSAN</label>
                <input type="text" name="jurusan" placeholder="Pilih Jurusan" class="form-control">
            </div>
            @if($errors->has('jurusan'))
                    <div class="text-danger">
                        {{ $errors->first('jurusan')}}
                    </div>
            @endif

            <div class="form-group">
                <label>MOTIVASI</label>
                <input type="text" name="motivasi" placeholder="Masukkan Motivasi" class="form-control">
            </div>
            @if($errors->has('motivasi'))
                    <div class="text-danger">
                        {{ $errors->first('motivasi')}}
                    </div>
            @endif

            <div class="form-group">
                <label>EMAIL</label>
                <input type="text" name="email" placeholder="Masukkan Email" class="form-control">
            </div>
            @if($errors->has('email'))
                    <div class="text-danger">
                        {{ $errors->first('email')}}
                    </div>
            @endif

            <div class="form-group">
                <label>STATUS</label>
                <select class="form-control" name="status">
                <option disabled hidden selected> Pilih Status </option>
                    <option value="DITERIMA"> Diterima
                    </option>
                    <option value="DITERIMA"> Tidak Diterima
                    </option>
                </select>
            @if($errors->has('status'))
                    <div class="text-danger">
                        {{ $errors->first('status')}}
                    </div>
            @endif

            <div class="form-group">
                <label>FILE</label>
                <form action="/action_page.php">
                <input type="file" name="file" class="form-control">
                </form>
                </div>
            @if($errors->has('file'))
                    <div class="text-danger">
                        {{ $errors->first('file')}}
                    </div>
            @endif
            

            {{-- <div class="form-group">
                <label></label>
                <input type="hidden" name="status" value="MENUNGGU" class="form-control">
            </div> --}}
        </div>
        <br>
        <div class="form-group">
            <button class="btn btn-success btn-sm"  style="margin-left: 8pt;" onclick="return confirm('Apakah data anda sudah benar?')"><i class="fa fa-plus"></i> &nbsp;TAMBAH</button>
            </div>
            
        </div>
    </div>

</form>
@if(Session::has(''))
    <script>
        toasts.success("{!! Session::get('') !!}");
    </script>
@endif

<script>
    $(document).ready(function() {
        $(".user12").select2({
            width: '100%'
        });
        
    });
</script>
@endsection