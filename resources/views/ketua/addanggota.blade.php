@extends('ketua.template.v_template')
@section('title','KEANGGOTAAN')
@section('content')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" integrity="sha512-wJgJNTBBkLit7ymC6vvzM1EcSWeM9mmOu+1USHaRBbHkm6W9EgM0HY27+UtUaprntaYQJF75rc8gjxllKs5OIQ==" crossorigin="anonymous" />

{{-- Header Form --}}
<div class="row">
    <div class="col-lg-9 margin-tb">
        <h3 class="pl-2" style="border-left: solid black 5px">&nbsp;Form Tambah Anggota</h3>
    </div>
</div>
<hr>
{{-- Header Form --}}

{{-- Awal Alert --}}
<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Silahkan mengisi data anggota</h4>
    Pada form yang telah disediakan berikut. . . 
</div>
{{-- Akhir Aler --}}


<form action="/ketua/anggota/store" method="POST" enctype="multipart/form-data">

    @csrf

      
    <div class="content">
        <div class="row">
            
            <div class="col-sm-6">
           
            <div class="form-group">
                <label>ID ANGGOTA</label>
                <input type="text" name="id_anggota" placeholder="Masukkan ID Anggota" class="form-control">
            </div>
            @if($errors->has('id_anggota'))
                    <div class="text-danger">
                        {{ $errors->first('id_anggota')}}
                    </div>
            @endif

            <div class="form-group">
                <label>NAMA ANGGOTA</label>
                <input type="text" name="nama_ang" placeholder="Masukkan Nama Anggota" class="form-control">
            </div>
            @if($errors->has('nama_ang'))
                    <div class="text-danger">
                        {{ $errors->first('nama_ang')}}
                    </div>
            @endif
            
            <div class="form-group">
                <label>JABATAN</label>
                <select class="form-control" required name="jabatan">
                <option disabled hidden selected> Pilih Jabatan </option>
                    <option value="KETUA"> KETUA
                    </option>
                    <option value="WAKIL KETUA"> WAKIL KETUA
                    </option>
                    <option value="SEKERTARIS"> SEKERTARIS
                    </option>
                    <option value="BENDAHARA"> BENDAHARA
                    </option>
                    <option value="ANGGOTA"> ANGGOTA
                    </option>
                </select>
            </div>
            @if($errors->has('jabatan'))
                    <div class="text-danger">
                        {{ $errors->first('jabatan')}}
                    </div>
            @endif

            <div class="form-group">
                <label>ALAMAT</label>
                <input type="text" name="alamat" placeholder="Masukkan Alamat" class="form-control">
            </div>
            @if($errors->has('alamat'))
                    <div class="text-danger">
                        {{ $errors->first('alamat')}}
                    </div>
            @endif

            <div class="form-group">
                <label>SEMESTER</label>
                <select class="form-control" required name="semester">
                <option disabled hidden selected> Pilih Semester </option>
                    <option value="I"> I
                    </option>
                    <option value="II"> II
                    </option>
                    <option value="III"> III
                    </option>
                    <option value="IV"> IV
                    </option>
                    <option value="V"> V
                    </option>
                    <option value="VI"> VI
                    </option>
                    </option>
                    <option value="VI"> VII
                    </option>
                    </option>
                    <option value="VI"> VIII
                    </option>
                </select>
            </div>
            @if($errors->has('semester'))
                    <div class="text-danger">
                        {{ $errors->first('semester')}}
                    </div>
            @endif
           
        </div>
            
        <div class="col-sm-6">
        <div class="form-group">
                <label>JURUSAN</label>
                <input type="text" name="jurusan" placeholder="Pilih Jurusan" class="form-control">
            </div>
            @if($errors->has('jurusan'))
                    <div class="text-danger">
                        {{ $errors->first('jurusan')}}
                    </div>
            @endif

            <div class="form-group">
                <label>KELAS</label>
                <select class="form-control" required name="kelas">
                <option disabled hidden selected> Pilih Kelas </option>
                    <option value="1"> 1
                    </option>
                    <option value="2"> 2
                    </option>
                    <option value="3"> 3
                    </option>
                    <option value="4"> 4
                    </option>
                </select>
            </div>
            @if($errors->has('kelas'))
                    <div class="text-danger">
                        {{ $errors->first('kelas')}}
                    </div>
            @endif

            <div class="form-group">
                <label>NO HP</label>
                <input type="text" name="no_hp" placeholder="Masukkan Nomor HP" class="form-control">
            </div>
            @if($errors->has('no_hp'))
                    <div class="text-danger">
                        {{ $errors->first('no_hp')}}
                    </div>
            @endif

            <div class="form-group">
                <label>EMAIL</label>
                <input type="text" name="email" placeholder="Masukkan Email" class="form-control">
            </div>
            @if($errors->has('email'))
                    <div class="text-danger">
                        {{ $errors->first('email')}}
                    </div>
            @endif

            <div class="form-group">
                <label>STATUS</label>
                <select class="form-control" name="status">
                <option disabled hidden selected> Pilih Status </option>
                    <option value="AKTIF"> AKTIF
                    </option>
                    <option value="NON-AKTIF"> NON-AKTIF
                    </option>
                </select>
            @if($errors->has('status'))
                    <div class="text-danger">
                        {{ $errors->first('status')}}
                    </div>
            @endif
            
            {{-- AKHIR ANGGOTA --}}
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js" integrity="sha512-zlWWyZq71UMApAjih4WkaRpikgY9Bz1oXIW5G0fED4vk14JjGlQ1UmkGM392jEULP8jbNMiwLWdM8Z87Hu88Fw==" crossorigin="anonymous"></script>
{{-- js --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js" integrity="sha512-zlWWyZq71UMApAjih4WkaRpikgY9Bz1oXIW5G0fED4vk14JjGlQ1UmkGM392jEULP8jbNMiwLWdM8Z87Hu88Fw==" crossorigin="anonymous"></script>

            {{-- <div class="form-group">
                <label></label>
                <input type="hidden" name="status" value="MENUNGGU" class="form-control">
            </div> --}}
        </div>
        <br>
        <div class="form-group">
            <button class="btn btn-success btn-sm"  style="margin-left: 8pt;" onclick="return confirm('Apakah data anda sudah benar?')"><i class="fa fa-plus"></i> &nbsp;TAMBAH</button>
            </div>
            
        </div>
    </div>

</form>
@if(Session::has(''))
    <script>
        toasts.success("{!! Session::get('') !!}");
    </script>
@endif

<script>
    $(document).ready(function() {
        $(".user12").select2({
            width: '100%'
        });
        
    });
</script>
@endsection