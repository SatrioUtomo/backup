@extends('pembimbing.template.v_template')
@section('title','PENDAFTAR')
@section('content')
    
<br>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" integrity="sha512-wJgJNTBBkLit7ymC6vvzM1EcSWeM9mmOu+1USHaRBbHkm6W9EgM0HY27+UtUaprntaYQJF75rc8gjxllKs5OIQ==" crossorigin="anonymous" />

{{-- Awal Alert --}}
    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> SELAMAT DATANG DI HALAMAN @yield('title')</h4>
        Pada halaman anda dapat melihat dan menambah data pendaftar GI - BEI. . . . . . 
    </div>
{{-- Akhir Alert --}}

{{-- AWAL Pendaftar --}}
<div class="container-fluid">
    <div class="row">
      <div class="col-lg-9 margin-tb">
          <h3 class="pl-2" style="border-left: solid black 5px">&nbsp;List Data Pendaftar</h3>
      </div>
  </div>
  <hr>

<!-- <div class="container" style="margin-left: -15px;">
    <a href="/pembimbing/daftar/tambah" class="btn btn-warning">+ Tambah Pendaftar</a>
 </div> -->
 <br>
 @if(Session::has('deleted'))
     <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{Session::get('deleted')}}
    </div>
 @endif

      <table class="table" id="table-pengajuan">
          <thead class="table" style="background-color: #18A558"  >
              <tr>
              <th style="color: white">ID PENDAFTAR</th>
              <th style="color: white">NAMA</th>
              <th style="color: white">ALAMAT</th>
              <th style="color: white">KELAS</th>
              <th style="color: white">JURUSAN</th>
              <th style="color: white">MOTIVASI</th>
              <th style="color: white">EMAIL</th>
              <th style="color: white">FILE</th>
              <th style="color: white">PILIHAN</th>
          </tr>
      </thead>
      <tbody>
      @foreach($daftar as $d)
          <tr>
                    <td>{{$d->id_daftar}}</td>
                    <td>{{$d->nama_daf}}</td>
                    <td>{{$d->alamat}}</td>
                    <td>{{$d->kelas}}</td>
                    <td>{{$d->jurusan}}</td>
                    <td>{{$d->motivasi}}</td>
                    <td>{{$d->email}}</td>
                    <td>{{$d->file}}</td>
              
                <td> 
                    <!-- <a href="/ketua/daftar/edit/{{ $d->id_daftar }}" class="btn btn-info btn-sm"><i class="fa fa-check"></i> Edit Data</a> &nbsp;
                    <a href="/ketua/daftar/hapus/{{ $d->id_daftar }}" onclick="return confirm('Apakah anda yakin untuk menghapus?')"  class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus Data</a> -->
                    <a href="#" class="btn btn-info btn-sm btnDetail"
                    data-iddaftar="{{$d->id_daftar}}"
                    data-namapendaftar="{{$d->nama_daf}}"
                    data-alamat="{{$d->alamat}}"
                    data-kelas="{{$d->kelas}}"
                    data-jurusan="{{$d->jurusan}}"
                    data-motivasi="{{$d->motivasi}}"
                    data-email="{{$d->email}}"
                    data-file="{{$d->file}}"
                    >
                        <i class="fa fa-eye"></i> Detail
                    </a>
                </td>    
              </tr>
          @endforeach
      </tbody>
  </table>
</div>
{{-- js --}}
{{-- Modal --}}

    <div class="modal modal-info fade" id="modal_anggota">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <div class="col-lg-9 margin-tb">
                <h3 class="pl-1" style="border-left: solid white 5px">&nbsp;Detail Pendaftar GI BEI</h3>
            </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <p>ID PENDAFTAR</p>
                        <input type="text" id="idpendaftar1">

                        <p>NAMA PENDAFTAR</p>
                        <input type="text" id="namapendaftar1">

                        <p>ALAMAT</p>
                        <input type="text" id="alamat1">

                        <p>KELAS</p>
                        <input type="text" id="kelas1">

                    </div>
                    <div class="col-md-6">

                        <p>JURUSAN</p>
                        <input type="text" id="jurusan1">

                        <p>MOTIVASI</p>
                        <input type="text" id="motivasi1">

                        <p>EMAIL</p>
                        <input type="text" id="email1">

                        <p>FILE</p>
                        <input type="text" id="file1">
                       
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
              
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
{{-- Akhir Modal --}}
<script>
  
    $(document).ready(function() {
        $('#table-pengajuan').DataTable({
            "columnDefs": [{
                "orderable": false,
                "searchable": true,
                "targets": 1
            }],
            "aLengthMenu": [
                [5, 10, 25, -1],
                [5, 10, 25, "All"]
            ],
            "iDisplayLength": 5
        });
    });
    /* Because i didnt set placeholder values in forms.py they will be set here using vanilla Javascript
    //We start indexing at one because CSRF_token is considered and input field
     */
    
    //Query All input fields
    var select_fields = document.getElementsByTagName('select')
    
    var input_fields = document.getElementsByTagName('input')
    
    
    for (var field in select_fields) {
        select_fields[field].className += ' form-control'
    }
    for (var field in input_fields) {
        input_fields[field].className += ' form-control'
    }
    </script>
{{-- js --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js" integrity="sha512-zlWWyZq71UMApAjih4WkaRpikgY9Bz1oXIW5G0fED4vk14JjGlQ1UmkGM392jEULP8jbNMiwLWdM8Z87Hu88Fw==" crossorigin="anonymous"></script>

{{-- AKHIR ANGGOTA --}}
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js" integrity="sha512-zlWWyZq71UMApAjih4WkaRpikgY9Bz1oXIW5G0fED4vk14JjGlQ1UmkGM392jEULP8jbNMiwLWdM8Z87Hu88Fw==" crossorigin="anonymous"></script>

<script>
    $('.btnDetail').click(function (e) { 
        e.preventDefault();
        console.log($(this).data('tanggal'));

        //text biasa
        // $('#tgl_detail').html('<hr>'+$(this).data('tanggal'));
        //text input
        $('#idpendaftar1').val($(this).data('iddaftar'));
        $('#namapendaftar1').val($(this).data('namapendaftar'));
        $('#alamat1').val($(this).data('alamat'));
        $('#kelas1').val($(this).data('kelas'));
        $('#jurusan1').val($(this).data('jurusan'));
        $('#motivasi1').val($(this).data('motivasi'));
        $('#email1').val($(this).data('email'));
        $('#file1').val($(this).data('file'));

        $('#modal_anggota').modal({
            backdrop: 'static',
            keyboard: false, // to prevent closing with Esc button (if you want this too)
            show: true
        })

    });
</script>

</body>
</html>
@endsection
