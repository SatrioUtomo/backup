  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu" data-widget="tree">
    <li class="header">PROKER ASLAB</li>
    <li class="{{ request()->is('pembimbing') ? 'active' : ''}}"><a href="/pembimbing"> <i class="fa fa-dashboard"></i> <span>DASHBOARD</span></a></li>
    <li class="{{ request()->is('pembimbing/pengajuan') ? 'active' : ''}}"><a href="/pembimbing/pengajuan"> <i class="fa fa-file"></i> <span>PENGAJUAN</span></a></li>
    <li class="treeview">
      <a href="/pembimbing">
        <i class="fa fa-dashboard"></i> <span>PROGRAM KERJA</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="/anggota"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
        <li class="{{ request()->is('anggota/proker') ? 'active' : ''}}"><a href="/anggota/proker"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
      </ul>
    </li>
   
    
    <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
    <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Komentar</span></a></li>

    {{-- KEANGGOTAAN --}}
    <li class="header">KEANGGOTAAN</li>
    <li class="{{ request()->is('pembimbing/anggota') ? 'active' : ''}}"><a href="/pembimbing/anggota"> <i class="fa fa-file"></i> <span>DATA ANGGOTA</span></a></li>
   
    <li class="{{ request()->is('pembimbing/struktur') ? 'active' : ''}}"><a href="/pembimbing/struktur"><i class="fa fa-users"></i> <span>DATA STRUKTUR</span></a></li>
    
    <li class="{{ request()->is('pembimbing/pendaftar') ? 'active' : ''}}"><a href="/pembimbing/daftar"><i class="fa fa-book"></i> <span>DATA PENDAFTAR</span></a></li>


  </ul>
    <li class="header">KEANGGOTAAN</li>

  </ul>