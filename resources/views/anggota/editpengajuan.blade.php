@extends('anggota.template.v_template')

@section('content')

{{-- Alert --}}
<div class="container">
    <h1><b>UPDATE PENGAJUAN</b></h1><br>
    <div class="alert alert-success" role="alert">
        Silakan anda mengupdate form. . . .
</div>
{{-- Alert --}}

    @foreach($pengajuan as $p)
    <form class="col-md-12" action="/anggota/pengajuan/edit" method="post">
    {{ csrf_field() }}
      <div class="hidden">
        <label for="id" class="form-label"><b></b></label>
        <input type="hidden" class="form-control" name="id" value="{{ $p->id }}" >
      </div> 
    
      <div class="row">
        <div class="form-group col-sm-6">
          <label for="id_user" class="form-label"><b>ID USER</b></label>
          {{-- <input type="text" class="form-control" name="id_user" value="{{ $p->id_user }}" > --}}
          <select class="user" name="id_user" class="form-control">
            @forelse ($user as $item)
               <option value="{{$item->id}}" {{ ($item->id == $p->id_user)? 'selected' : ''}}>{{$item->name}}</option>
               @empty
               <option value="0">data kosong</option>
            @endforelse
        </select>
      
        
          <label for="nama_proker" class="form-label"><b>PROGRAM KERJA</b></label>
          <input type="text" class="form-control" name="nama_proker" value="{{ $p->nama_proker }}" >
       
         
        </div>
      
        <div class="form-group col-sm-6">
          <label for="detail" class="form-label"><b>DETAIL </b></label>
          <input type="text" class="form-control" name="detail" value="{{ $p->detail }}" >
        
          <label for="status" class="form-label"><b>STATUS</b></label>
          <input type="text" class="form-control" name="status" value="{{ $p->status }}" >
        
      
          <label for="keterangan" class="form-label"><b>KETERANGAN</b></label>
          <input type="text" class="form-control" name="keterangan" value="{{ $p->keterangan }}" >
        
      
     
    
      <button type="submit" class="btn btn-info"><i class="fa fa-edit"></i> &nbsp; UPDATE</button> &nbsp;
      <a href="{{ URL::previous() }}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> &nbsp; KEMBALI</a>
    
    
    </div>
    </div>
    </form>
    @endforeach
    
    <script>
      $(document).ready(function() {
          $(".user").select2({
              width: '100%'
          });
          
      });
  </script>
@endsection