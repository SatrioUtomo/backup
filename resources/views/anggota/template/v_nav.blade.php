  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu" data-widget="tree">
    <li class="header">PROKER ASLAB</li>
    <li class="{{ request()->is('anggota') ? 'active' : ''}}"><a href="/anggota"> <i class="fa fa-dashboard"></i> <span>DASHBOARD</span></a></li>
    <li class="{{ request()->is('anggota/pengajuan') ? 'active' : ''}}"><a href="/anggota/pengajuan"> <i class="fa fa-file"></i> <span>PENGAJUAN</span></a></li>
    <li class="treeview">
      <a href="/anggota">
        <i class="fa fa-dashboard"></i> <span>PROGRAM KERJA</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="/anggota"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
        <li class="{{ request()->is('anggota/proker') ? 'active' : ''}}"><a href="/anggota/proker"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
      </ul>
    </li>
   
    <li class="treeview">
      <a href="#">
        <i class="fa fa-share"></i> <span>Multilevel</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
        <li class="treeview">
          <a href="#"><i class="fa fa-circle-o"></i> Level One
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Level Two
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
              </ul>
            </li>
          </ul>
        </li>
        <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
      </ul>
    </li>
    <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>

    <!-- {{-- KEANGGOTAAN --}}
    <li class="header">KEANGGOTAAN</li>
    <li class="{{ request()->is('anggota/anggota') ? 'active' : ''}}"><a href="/anggota/anggota"> <i class="fa fa-file"></i> <span>KELOLA ANGGOTA</span></a></li>
   
    <li class="{{ request()->is('anggota/struktur') ? 'active' : ''}}"><a href="/anggota/struktur"><i class="fa fa-users"></i> <span>KELOLA STRUKTUR</span></a></li>
    
    <li class="{{ request()->is('anggota/daftar') ? 'active' : ''}}"><a href="/anggota/daftar"><i class="fa fa-book"></i> <span>DATA PENDAFTAR</span></a></li>
 -->

  </ul>