@extends('anggota.template.v_template')
@section('title','KEANGGOTAAN')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" integrity="sha512-wJgJNTBBkLit7ymC6vvzM1EcSWeM9mmOu+1USHaRBbHkm6W9EgM0HY27+UtUaprntaYQJF75rc8gjxllKs5OIQ==" crossorigin="anonymous" />

</head>
<body>
      
<br>

{{-- Awal Alert --}}
    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> SELAMAT DATANG DI HALAMAN @yield('title')</h4>
        Pada halaman anda dapat melihat dan menambah data anggota GI - BEI. . . 
    </div>
{{-- Akhir Aler --}}

{{-- AWAL ANGGOTA --}}
<div class="container-fluid">
    <div class="row">
      <div class="col-lg-9 margin-tb">
          <h3 class="pl-2" style="border-left: solid black 5px">&nbsp;List Data Anggota</h3>
      </div>
    </div>
  <hr>

<div class="container" style="margin-left: -15px;">
    <a href="/anggota/anggota/tambah" class="btn btn-warning">+ Tambah Anggota</a>
 </div>
 <br>
 @if(Session::has('deleted'))
     <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{Session::get('deleted')}}
    </div>
 @endif

 <div class="table-responsive">

 
      <table class="table" id="table-pengajuan">
          <thead class="table" style="background-color: #18A558"  >
              <tr>
            <th style="color: white">NIM</th>
            <th style="color: white">NAMA</th>
            <th style="color: white">JABATAN</th>
            <th style="color: white">ALAMAT</th>
            <th style="color: white">SEMESTER</th>
            <th style="color: white">JURUSAN</th>
            <th style="color: white">KELAS</th>
            <th style="color: white">NO_HP</th>
            <th style="color: white">EMAIL</th>
            <th style="color: white">STATUS</th>
            <th style="color: white">PILIHAN</th>
          </tr>
      </thead>
      <tbody>
          @foreach($anggota as $p)
          <tr>
                    <td>{{$p->id_anggota}}</td>
                    <td>{{$p->nama_ang}}</td>
                    <td>{{$p->jabatan}}</td>
                    <td>{{$p->alamat}}</td>
                    <td>{{$p->semester}}</td>
                    <td>{{$p->jurusan}}</td>
                    <td>{{$p->kelas}}</td>
                    <td>{{$p->no_hp}}</td>
                    <td>{{$p->email}}</td>
                    <td>{{$p->status}}</td>
              
                <td> 
                    <a href="/anggota/anggota/edit/{{ $p->id_anggota }}" class="btn btn-info btn-sm"><i class="fa fa-check"></i> Edit Data</a> &nbsp;
                    <a href="/anggota/anggota/hapus/{{ $p->id_anggota }}" onclick="return confirm('Apakah anda yakin untuk menghapus?')"  class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus Data</a>
                    <a href="#" class="btn btn-info btn-sm btnDetail"
                    data-idanggota="{{$p->id_anggota}}"
                    data-namaanggota="{{$p->nama_ang}}"
                    data-jabatan="{{$p->jabatan}}"
                    data-alamat="{{$p->alamat}}"

                    data-semester="{{$p->semester}}"
                    data-jurusan="{{$p->jurusan}}"
                    data-kelas="{{$p->kelas}}"
                    data-nohp="{{$p->no_hp}}"
                    data-email="{{$p->email}}"
                    data-status="{{$p->status}}"
                    >
                        <i class="fa fa-eye"></i> Detail
                    </a>
                </td>    
              </tr>
          @endforeach
      </tbody>
  </table>
</div>
</div>
{{-- js --}}
 {{-- Modal --}}

    <div class="modal modal-info fade" id="modal_anggota">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <div class="col-lg-9 margin-tb">
                <h3 class="pl-1" style="border-left: solid white 5px">&nbsp;Detail Anggota GI BEI</h3>
            </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <p>ID ANGGOTA</p>
                        <input type="text" id="idanggota1">

                        <p>NAMA ANGGOTA</p>
                        <input type="text" id="namaanggota1">

                        <p>JABATAN</p>
                        <input type="text" id="jabatan1">

                        <p>ALAMAT</p>
                        <input type="text" id="alamat1">

                        <p>SEMESTER</p>
                        <input type="text" id="semester1">


                    </div>
                    <div class="col-md-6">

                    <p>JURUSAN</p>
                        <input type="text" id="jurusan1">

                        <p>KELAS</p>
                        <input type="text" id="kelas1">

                        <p>NO HP</p>
                        <input type="text" id="nohp1">

                        <p>EMAIL</p>
                        <input type="text" id="email1">

                        <p>STATUS</p>
                        <input type="text" id="status1">
                       
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
{{-- Akhir Modal --}}

<script>
    $(document).ready(function() {
        $('#table-pengajuan').DataTable({
            "columnDefs": [{
                "orderable": false,
                "searchable": true,
                "targets": 1
            }],
            "aLengthMenu": [
                [5, 10, 25, -1],
                [5, 10, 25, "All"]
            ],
            "iDisplayLength": 5
        });
    });
    /* Because i didnt set placeholder values in forms.py they will be set here using vanilla Javascript
    //We start indexing at one because CSRF_token is considered and input field
     */
    
    //Query All input fields
    var select_fields = document.getElementsByTagName('select')
    
    var input_fields = document.getElementsByTagName('input')
    
    
    for (var field in select_fields) {
        select_fields[field].className += ' form-control'
    }
    for (var field in input_fields) {
        input_fields[field].className += ' form-control'
    }
    </script>
{{-- js --}}

{{-- AKHIR RAPAT --}}
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js" integrity="sha512-zlWWyZq71UMApAjih4WkaRpikgY9Bz1oXIW5G0fED4vk14JjGlQ1UmkGM392jEULP8jbNMiwLWdM8Z87Hu88Fw==" crossorigin="anonymous"></script>

@if(Session::has('deleted'))
    <script>
        toasts.success("{!! Session::get('deleted') !!}");
    </script>
@endif

<script>
    $('.btnDetail').click(function (e) { 
        e.preventDefault();
        console.log($(this).data('tanggal'));

        //text biasa
        // $('#tgl_detail').html('<hr>'+$(this).data('tanggal'));
        //text input
        $('#idanggota1').val($(this).data('idanggota'));
        $('#namaanggota1').val($(this).data('namaanggota'));
        $('#jabatan1').val($(this).data('jabatan'));
        $('#alamat1').val($(this).data('alamat'));
        $('#semester1').val($(this).data('semester'));
        $('#jurusan1').val($(this).data('jurusan'));
        $('#kelas1').val($(this).data('kelas'));
        $('#nohp1').val($(this).data('nohp'));
        $('#email1').val($(this).data('email'));
        $('#status1').val($(this).data('status'));

        $('#modal_anggota').modal({
            backdrop: 'static',
            keyboard: false, // to prevent closing with Esc button (if you want this too)
            show: true
        })

    });
</script>

</body>
</html>
@endsection
