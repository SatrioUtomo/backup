@extends('anggota.template.v_template')

@section('content')

{{-- Header Form --}}
<div class="row">
    <div class="col-lg-9 margin-tb">
        <h3 class="pl-2" style="border-left: solid black 5px">&nbsp;Form Tambah Anggota</h3>
    </div>
</div>
<hr>
{{-- Header Form --}}

{{-- Awal Alert --}}
<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> silahkan mengisi data anggota</h4>
    Pada form yang telah disediakan berikut. . . 
</div>
{{-- Akhir Aler --}}


<form action="/anggota/anggota/store" method="POST" enctype="multipart/form-data">

    @csrf

      
    <div class="content">
        <div class="row">
            
            <div class="col-sm-6">
           
            <div class="form-group">
                <label>ID ANGGOTA</label>
                <input type="text" name="id_anggota" class="form-control">
            </div>
            @if($errors->has('id_anggota'))
                    <div class="text-danger">
                        {{ $errors->first('id_anggota')}}
                    </div>
            @endif

            <div class="form-group">
                <label>NAMA ANGGOTA</label>
                <input type="text" name="nama_ang" class="form-control">
            </div>
            @if($errors->has('nama_ang'))
                    <div class="text-danger">
                        {{ $errors->first('nama_ang')}}
                    </div>
            @endif
            
            <div class="form-group">
                <label>JABATAN</label>
                <input type="text" name="jabatan" class="form-control">
            </div>
            @if($errors->has('jabatan'))
                    <div class="text-danger">
                        {{ $errors->first('jabatan')}}
                    </div>
            @endif

            <div class="form-group">
                <label>ALAMAT</label>
                <input type="text" name="alamat" class="form-control">
            </div>
            @if($errors->has('alamat'))
                    <div class="text-danger">
                        {{ $errors->first('alamat')}}
                    </div>
            @endif

            <div class="form-group">
                <label>SEMESTER</label>
                <input type="text" name="semester" class="form-control">
            </div>
            @if($errors->has('semester'))
                    <div class="text-danger">
                        {{ $errors->first('semester')}}
                    </div>
            @endif

            
            
           
        </div>
        
            
        <div class="col-sm-6">
        <div class="form-group">
                <label>JURUSAN</label>
                <input type="text" name="jurusan" class="form-control">
            </div>
            @if($errors->has('jurusan'))
                    <div class="text-danger">
                        {{ $errors->first('jurusan')}}
                    </div>
            @endif

            <div class="form-group">
                <label>KELAS</label>
                <input type="text" name="kelas" class="form-control">
            </div>
            @if($errors->has('kelas'))
                    <div class="text-danger">
                        {{ $errors->first('kelas')}}
                    </div>
            @endif

            <div class="form-group">
                <label>NO HP</label>
                <input type="text" name="no_hp" class="form-control">
            </div>
            @if($errors->has('no_hp'))
                    <div class="text-danger">
                        {{ $errors->first('no_hp')}}
                    </div>
            @endif

            <div class="form-group">
                <label>EMAIL</label>
                <input type="text" name="email" class="form-control">
            </div>
            @if($errors->has('email'))
                    <div class="text-danger">
                        {{ $errors->first('email')}}
                    </div>
            @endif

            <div class="form-group">
                <label>STATUS</label>
                <select class="form-control" name="status">
                    <option value="AKTIF"> AKTIF
                    </option>
                    <option value="NON-AKTIF"> NON-AKTIF
                    </option>
                </select>
            @if($errors->has('status'))
                    <div class="text-danger">
                        {{ $errors->first('status')}}
                    </div>
            @endif
            

            {{-- <div class="form-group">
                <label></label>
                <input type="hidden" name="status" value="MENUNGGU" class="form-control">
            </div> --}}
        </div>
        <br>
        <div class="form-group">
            <button class="btn btn-success btn-sm"  style="margin-left: 8pt;" onclick="return confirm('Apakah data anda sudah benar?')"><i class="fa fa-plus"></i> &nbsp;TAMBAH</button>
            </div>
            
        </div>
    </div>

</form>
@if(Session::has(''))
    <script>
        toasts.success("{!! Session::get('') !!}");
    </script>
@endif

<script>
    $(document).ready(function() {
        $(".user12").select2({
            width: '100%'
        });
        
    });
</script>
@endsection