@extends('anggota.template.v_template')

@section('content')

{{-- Header Form --}}
<div class="row">
    <div class="col-lg-9 margin-tb">
        <h3 class="pl-2" style="border-left: solid black 5px">&nbsp;Form Pengajuan Program Kerja</h3>
    </div>
</div>
<hr>
{{-- Header Form --}}

{{-- Awal Alert --}}
<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> silakan mengisi data pengajuan program kerja</h4>
    Pada form yang telah disediakan berikut. . . 
</div>
{{-- Akhir Aler --}}


<form action="/anggota/pengajuan/store" method="POST" enctype="multipart/form-data">

    @csrf

      
    <div class="content">
        <div class="row">
            
            <div class="col-sm-6">
           
           
             
             <div class="form-group">
                 <label>ID USER</label>
                 <select class="user12" name="id_user" class="form-control">
                     @forelse ($user as $item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                        @empty
                        <option value="0">data kosong</option>
                     @endforelse
                 </select>
                </div>
                
            @if($errors->has('id_user'))
                    <div class="text-danger">
                        {{ $errors->first('id_user')}}
                    </div>
            @endif

            <div class="form-group">
                <label>PROGRAM KERJA</label>
                <input type="text" name="nama_proker" class="form-control">
            </div>
            @if($errors->has('nama_proker'))
                    <div class="text-danger">
                        {{ $errors->first('nama_proker')}}
                    </div>
            @endif
            
           
        </div>
        
            
        <div class="col-sm-6">
            <div class="form-group">
                <label>STATUS PROKER</label>
                <select class="form-control" name="status">
                    <option value="DITERIMA"> DITERIMA
                    </option>
                    <option value="DITOLAK"> DITOLAK
                    </option>
                </select>
            </div>
            @if($errors->has('status'))
                    <div class="text-danger">
                        {{ $errors->first('status')}}
                    </div>
            @endif
            
            <div class="form-group">
                <label>KETERANGAN</label>
                <input type="text" name="keterangan" class="form-control">
            </div>
            @if($errors->has('keterangan'))
                    <div class="text-danger">
                        {{ $errors->first('keterangan')}}
                    </div>
            @endif
            
            <div class="form-group">
                <label>DETAIL PROKER</label>
                <input type="text" name="detail" class="form-control">
            </div>
            @if($errors->has('detail'))
                    <div class="text-danger">
                        {{ $errors->first('detail')}}
                    </div>
            @endif

            {{-- <div class="form-group">
                <label></label>
                <input type="hidden" name="status" value="MENUNGGU" class="form-control">
            </div> --}}
        </div>
        <br>
        <div class="form-group">
            <button class="btn btn-success btn-sm"  style="margin-left: 8pt;" onclick="return confirm('Apakah data anda sudah benar?')"><i class="fa fa-plus"></i> &nbsp;TAMBAH</button>
            </div>
            
        </div>
    </div>

</form>
@if(Session::has(''))
    <script>
        toasts.success("{!! Session::get('') !!}");
    </script>
@endif

<script>
    $(document).ready(function() {
        $(".user12").select2({
            width: '100%'
        });
        
    });
</script>
@endsection