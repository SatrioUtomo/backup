<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PengajuanController;
use App\Http\Controllers\PanitiaController;
use App\Http\Controllers\RapatController;
use App\Http\Controllers\ProkerController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AnggotaController;
use App\Http\Controllers\DaftarController;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//===============================ketua==================================================
//ketika membuka halaman ketua, akan muncul atau 
//mengambil view pada index.blade.php pada folder ketua
Route::view('/ketua','ketua/index');

// *********KETUA PENGAJUAN********************

// fungsi untuk menampilkan data dengan mengambil class index di pengajuancontroller
Route::get('/ketua/pengajuan', [PengajuanController::class, 'indexket']);

// fungsi untuk menampilkan form edit data menggunakan get
Route::get('/ketua/pengajuan/edit/{id}', [PengajuanController::class, 'editket']);

// fungsi untuk memproses edit data menggunakan get
Route::post('/ketua/pengajuan/edit', [PengajuanController::class, 'updateket']);

// fungsi untuk memproses hapus
Route::get('/ketua/pengajuan/hapus/{id}', [PengajuanController::class, 'hapusket']);


// ###################### K E T U A P R O K E R ###################################
Route::view('/ketua/proker','ketua/proker');

// fungsi untuk menampilkan data dengan mengambil class index di prokercontroller
Route::get('/ketua/proker', [ProkerController::class, 'index']);

// fungsi untuk menampilkan form tambah data dengan mengambil class tambah di prokerncontroller
Route::get('/ketua/proker/tambah', [ProkerController::class, 'tambah']);

// fungsi untuk memproses tambah data dengan mengambil class store di prokercontroller
Route::post('/ketua/proker/store', [ProkerController::class, 'store']);

// fungsi untuk menampilkan form edit data menggunakan get
Route::get('/ketua/proker/edit/{id}', [ProkerController::class, 'edit']);

// fungsi untuk memproses edit data menggunakan get
Route::post('/ketua/proker/edit', [ProkerController::class, 'update']);

// fungsi untuk memproses hapus
Route::get('/ketua/proker/hapus/{id}', [ProkerController::class, 'hapus']);


// ###################### K E T U A R A P A T ###################################
Route::view('/ketua/rapat','ketua/rapat');

// fungsi untuk menampilkan data dengan mengambil class index di rapatcontroller
Route::get('/ketua/rapat', [RapatController::class, 'index']);

// fungsi untuk memproses hapus
Route::get('/ketua/rapat/hapus/{id_rapat}', [RapatController::class, 'hapus']);


// ##################### K E T U A P A N I T I A ###############################
Route::view('/ketua/panitia','ketua/panitia');
// fungsi untuk menampilkan data dengan mengambil class index di pengajuancontroller
Route::get('/ketua/panitia', [PanitiaController::class, 'index']);

// fungsi untuk menampilkan form tambah data dengan mengambil class tambah di pengajuancontroller
Route::get('/ketua/panitia/tambah', [PanitiaController::class, 'tambah']);

// fungsi untuk memproses tambah data dengan mengambil class store di pengajuancontroller
Route::post('/ketua/panitia/store', [PanitiaController::class, 'store']);

// fungsi untuk menampilkan form edit data menggunakan get
Route::get('/ketua/panitia/edit/{id}', [PanitiaController::class, 'edit']);

// fungsi untuk memproses edit data menggunakan get
Route::post('/ketua/panitia/edit', [PanitiaController::class, 'update']);

// fungsi untuk memproses hapus
Route::get('/ketua/panitia/hapus/{id}', [PanitiaController::class, 'hapus']);

Route::get('/ketua/panitia/dataTable', [PanitiaController::class, 'dataTable']);
// ***************************************************************************************

// KELOLA USER
Route::get('/ketua/user', [UserController::class, 'index']);

// ###################### K E T U A A N G G O T A ###################################
Route::view('/ketua/anggota','ketua/anggota');

// fungsi untuk menampilkan data dengan mengambil class index di anggotacontroller
Route::get('/ketua/anggota', [AnggotaController::class, 'indexang']);

// fungsi untuk menampilkan form tambah data dengan mengambil class tambah di anggotacontroller
Route::get('/ketua/anggota/tambah', [AnggotaController::class, 'tambahang']);

// fungsi untuk memproses tambah data dengan mengambil class store di anggotacontroller
Route::post('/ketua/anggota/store', [AnggotaController::class, 'storeang']);

// fungsi untuk menampilkan form edit data menggunakan get
Route::get('/ketua/anggota/edit/{id_anggota}', [AnggotaController::class, 'editang']);

// fungsi untuk memproses edit data menggunakan get
Route::post('/ketua/anggota/edit', [AnggotaController::class, 'updateang']);

// fungsi untuk memproses hapus
Route::get('/ketua/anggota/hapus/{id_anggota}', [AnggotaController::class, 'hapusang']);
// ***************************************************************************************


// ###################### K E T U A S T R U K T U R ###################################
Route::view('/ketua/struktur','ketua/struktur');


// ###################### K E T U A P E N D A F T A R ###################################
Route::view('/ketua/daftar','ketua/daftar');

// fungsi untuk menampilkan data dengan mengambil class index di daftarcontroller
Route::get('/ketua/daftar', [DaftarController::class, 'index']);

// fungsi untuk menampilkan form tambah data dengan mengambil class tambah di DaftarController
Route::get('/ketua/daftar/tambah', [DaftarController::class, 'tambah']);

// fungsi untuk memproses tambah data dengan mengambil class store di DaftarController
Route::post('/ketua/daftar/store', [DaftarController::class, 'store']);

// fungsi untuk menampilkan form edit data menggunakan get
Route::get('/ketua/daftar/edit/{id_daftar}', [DaftarController::class, 'edit']);

// fungsi untuk memproses edit data menggunakan get
Route::post('/ketua/daftar/edit', [DaftarController::class, 'update']);

// fungsi untuk memproses hapus
Route::get('/ketua/daftar/hapus/{id_daftar}', [DaftarController::class, 'hapus']);
// ***************************************************************************************

// ###################### K E T U A J U RU S A N ###################################
Route::view('/ketua/jurusan','ketua/jurusan');

//===========================ANGGOTA=======================================================
// DASHBOARD
Route::view('/anggota','anggota/index');

// *********PENGAJUAN********************
Route::view('/anggota/pengajuan','anggota/pengajuan');

// fungsi untuk menampilkan data dengan mengambil class index di pengajuancontroller
Route::get('/anggota/pengajuan', [PengajuanController::class, 'index']);

// fungsi untuk menampilkan form tambah data dengan mengambil class tambah di pengajuancontroller
Route::get('/anggota/pengajuan/tambah', [PengajuanController::class, 'tambah']);

// fungsi untuk memproses tambah data dengan mengambil class store di pengajuancontroller
Route::post('/anggota/pengajuan/store', [PengajuanController::class, 'store']);

// fungsi untuk menampilkan form edit data menggunakan get
Route::get('/anggota/pengajuan/edit/{id}', [PengajuanController::class, 'edit']);

// fungsi untuk memproses edit data menggunakan get
Route::post('/anggota/pengajuan/edit', [PengajuanController::class, 'update']);

// fungsi untuk memproses hapus
Route::get('/anggota/pengajuan/hapus/{id}', [PengajuanController::class, 'hapus']);
// ******************************AKHIR PENGAJUAN*************************************

Route::view('/anggota/proker','anggota/proker');
Route::view('/coba','anggota.template.v_template');


// *********KEANGGOTAAN********************
// fungsi untuk menampilkan data dengan mengambil class index di anggotacontroller
Route::get('/anggota/anggota', [AnggotaController::class, 'index']);

// fungsi untuk menampilkan form tambah data dengan mengambil class tambah di anggotacontroller
Route::get('/anggota/anggota/tambah', [AnggotaController::class, 'tambah']);

// fungsi untuk memproses tambah data dengan mengambil class store di anggotacontroller
Route::post('/anggota/anggota/store', [AnggotaController::class, 'store']);

// fungsi untuk menampilkan form edit data menggunakan get
Route::get('/anggota/anggota/edit/{id_anggota}', [AnggotaController::class, 'edit']);

// fungsi untuk memproses edit data menggunakan get
Route::post('/anggota/anggota/edit', [AnggotaController::class, 'update']);

// fungsi untuk memproses hapus
Route::get('/anggota/anggota/hapus/{id_anggota}', [AnggotaController::class, 'hapus']);
// ******************************AKHIR KEANGGOTAAN*************************************

// ***************************************************************************************


//==========================PEMBIMBING==================================================
Route::view('/pembimbing','pembimbing/index');

// *********ANGGOTA********************
// fungsi untuk menampilkan data dengan mengambil class index di anggotacontroller
Route::get('/pembimbing/anggota', [AnggotaController::class, 'indexpem']);

// fungsi untuk menampilkan form tambah data dengan mengambil class tambah di anggotacontroller
Route::get('/pembimbing/anggota/tambah', [AnggotaController::class, 'tambahpem']);

// fungsi untuk memproses tambah data dengan mengambil class store di anggotacontroller
Route::post('/pembimbing/anggota/store', [AnggotaController::class, 'storepem']);

// fungsi untuk menampilkan form edit data menggunakan get
Route::get('/pembimbing/anggota/edit/{id_anggota}', [AnggotaController::class, 'editpem']);

// fungsi untuk memproses edit data menggunakan get
Route::post('/pembimbing/anggota/edit', [AnggotaController::class, 'updatepem']);

// fungsi untuk memproses hapus
Route::get('/pembimbing/anggota/hapus/{id_anggota}', [AnggotaController::class, 'hapuspem']);
// ***************************************************************************************

// *************PENDAFTAR****************
// fungsi untuk menampilkan data dengan mengambil class index di anggotacontroller
Route::get('/pembimbing/daftar', [DaftarController::class, 'indexpem']);

Auth::routes();
Route::post('/kirim_login', [LoginController::class, 'proses_login'])->name('kirim_login');
Route::get('/upd_pass', [LoginController::class, 'upd_pass'])->name('upd_pass');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
