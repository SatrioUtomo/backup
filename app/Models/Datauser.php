<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Datauser extends Model
{
    use HasFactory;
       //menggunakan softdelete
       use SoftDeletes;

       //nama tabel yang akan digunakan yaitu tabel user
       protected $table = 'user';
       
       //kolom tabel yang boleh diisi
       protected $fillable = ['id','id_user','id_proker','jabatan'];
}
