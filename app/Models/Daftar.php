<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Daftar extends Model
{
    use HasFactory;

    //menggunakan softdelete
    use SoftDeletes;

    //nama tabel yang akan digunakan yaitu tabel daftar
    protected $table = 'daftar';
    
    //kolom tabel yang boleh diisi
    protected $fillable = ['id_daftar','nama_daf','alamat','kelas','jurusan','motivasi','email','status','file'];

}
