<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Anggota extends Model
{
    use HasFactory;

    //menggunakan softdelete
    use SoftDeletes;

    //nama tabel yang akan digunakan yaitu tabel anggota
    protected $table = 'anggota';
    
    //kolom tabel yang boleh diisi
    protected $fillable = ['id_anggota','nama_ang','jabatan','alamat','semester','jurusan','kelas','no_hp','email','status'];

}
