<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Anggota;
use Illuminate\Support\Facades\DB;

class AnggotaController extends Controller
{
// -----------Awal  - Kelola - Anggota--------------------------------------------------

    //membuat fungsi untuk menampilkan data dari database ke dalam laravel
    public function index()
    {
        //mengambil data anggota dengan fungsi all (semua data)
    	$anggota = Anggota::all();
        // $pengajuan = Pengajuan::where('status','diterima')->get();

        //menampilkan data ke view anggota/anggota 
    	return view('anggota.anggota', compact('anggota'));
    }

    //membuat fungsi untuk beralih ke halaman form tambah anggota
    public function tambah()
    {
        //menampilkan halaman form addanggota
        return view('anggota.addanggota');
    }

    //fungsi untuk membuat proses inputan data
    public function store(Request $request)
    {
        //validasi untuk mengisi kolom
        $this->validate($request,[
            'id_anggota' => 'required',
            'nama_ang' => 'required',
            'jabatan' => 'required',
            'alamat' => 'required',
            'semester' => 'required',
            'jurusan' => 'required',
            'kelas' => 'required',
            'no_hp' => 'required',
            'email' => 'required',
            'status' => 'required'
        ]);
        //fungsi untuk proses inputan data ke database
        Anggota::create([
            'id_anggota' => $request->id_anggota,
            'nama_ang' => $request->nama_ang,
            'jabatan' => $request->jabatan,
            'alamat' => $request->alamat,
            'semester' => $request->semester,
            'jurusan' => $request->jurusan,
            'kelas' => $request->kelas,
            'no_hp' => $request->no_hp,
            'email' => $request->email,
            'status' => $request->status,
        ]);
        return redirect('anggota/anggota');
    }

    //fungsi untuk menampilkan form edit
    public function edit($id_anggota)
    {
        $anggota = DB::table('anggota')->where('id_anggota',$id_anggota)->get();
        return view('anggota.editanggota', compact('anggota'));
    }
    //membuat fungsi untuk proses update
    public function update(Request $request, Anggota $anggota)
    {
        DB::table('anggota')->where('id_anggota',$request->id_anggota)->update([
            'nama_ang' => $request->nama_ang,
            'jabatan' => $request->jabatan,
            'alamat' => $request->alamat,
            'semester' => $request->semester,
            'jurusan' => $request->jurusan,
            'kelas' => $request->kelas,
            'no_hp' => $request->no_hp,
            'email' => $request->email,
            'status' => $request->status
        ]);

        // anggota::find($request->id);
        return redirect('anggota/anggota');
    }

    //membuat fungsi untuk memproses hapus
    public function hapus($id_anggota)
    {
        Anggota::where('id_anggota',$id_anggota)->delete();
        return back()->with('deleted','Data Anggota Berhasil Terhapus!!!');
    }

// -----------Akhir - Keanggotaan - Anggota--------------------------------------------------


// -----------Awal - Keanggotaan - Ketua--------------------------------------------------
    public function indexang()
    {
   //mengambil data anggota dengan fungsi all (semua data)
   $anggota = Anggota::all();
   // $pengajuan = Pengajuan::where('status','diterima')->get();

   //menampilkan data ke view anggota/anggota 
   return view('ketua.anggota', compact('anggota'));
    }

     //membuat fungsi untuk beralih ke halaman form tambah anggota
     public function tambahang()
     {
         //menampilkan halaman form addanggota
         return view('ketua.addanggota');
     }
 
     //fungsi untuk membuat proses inputan data
     public function storeang(Request $request)
     {
         //validasi untuk mengisi kolom
         $this->validate($request,[
             'id_anggota' => 'required',
             'nama_ang' => 'required',
             'jabatan' => 'required',
             'alamat' => 'required',
             'semester' => 'required',
             'jurusan' => 'required',
             'kelas' => 'required',
             'no_hp' => 'required',
             'email' => 'required',
             'status' => 'required'
         ]);
         //fungsi untuk proses inputan data ke database
         Anggota::create([
             'id_anggota' => $request->id_anggota,
             'nama_ang' => $request->nama_ang,
             'jabatan' => $request->jabatan,
             'alamat' => $request->alamat,
             'semester' => $request->semester,
             'jurusan' => $request->jurusan,
             'kelas' => $request->kelas,
             'no_hp' => $request->no_hp,
             'email' => $request->email,
             'status' => $request->status,
         ]);
         return redirect('ketua/anggota');
     }

        //fungsi untuk menampilkan form edit
    public function editang($id_anggota)
    {
        $anggota = DB::table('anggota')->where('id_anggota',$id_anggota)->get();
        return view('ketua.editanggota', compact('anggota'));
    }
    //membuat fungsi untuk proses update
    public function updateang(Request $request, Anggota $anggota)
    {
        DB::table('anggota')->where('id_anggota',$request->id_anggota)->update([
            'nama_ang' => $request->nama_ang,
            'jabatan' => $request->jabatan,
            'alamat' => $request->alamat,
            'semester' => $request->semester,
            'jurusan' => $request->jurusan,
            'kelas' => $request->kelas,
            'no_hp' => $request->no_hp,
            'email' => $request->email,
            'status' => $request->status
        ]);

        // anggota::find($request->id);
        return redirect('ketua/anggota');
    }

    //membuat fungsi untuk memproses hapus
    public function hapusang($id_anggota)
    {
        Anggota::where('id_anggota',$id_anggota)->delete();
        return back()->with('deleted','Data Anggota Berhasil Terhapus!!!');
    }

// -----------Akhir - Pengajuan - Ketua--------------------------------------------------


// -----------Awal - Keanggotaan - Pembimbing--------------------------------------------------
public function indexpem()
{
//mengambil data anggota dengan fungsi all (semua data)
$anggota = Anggota::all();
// $pengajuan = Pengajuan::where('status','diterima')->get();

//menampilkan data ke view anggota/anggota 
return view('pembimbing.anggota', compact('anggota'));
}

 //membuat fungsi untuk beralih ke halaman form tambah anggota
 public function tambahpem()
 {
     //menampilkan halaman form addanggota
     return view('pembimbing.addanggota');
 }

 //fungsi untuk membuat proses inputan data
 public function storepem(Request $request)
 {
     //validasi untuk mengisi kolom
     $this->validate($request,[
         'id_anggota' => 'required',
         'nama_ang' => 'required',
         'jabatan' => 'required',
         'alamat' => 'required',
         'semester' => 'required',
         'jurusan' => 'required',
         'kelas' => 'required',
         'no_hp' => 'required',
         'email' => 'required',
         'status' => 'required'
     ]);
     //fungsi untuk proses inputan data ke database
     Anggota::create([
         'id_anggota' => $request->id_anggota,
         'nama_ang' => $request->nama_ang,
         'jabatan' => $request->jabatan,
         'alamat' => $request->alamat,
         'semester' => $request->semester,
         'jurusan' => $request->jurusan,
         'kelas' => $request->kelas,
         'no_hp' => $request->no_hp,
         'email' => $request->email,
         'status' => $request->status,
     ]);
     return redirect('pembimbing/anggota');
 }

    //fungsi untuk menampilkan form edit
public function editpem($id_anggota)
{
    $anggota = DB::table('anggota')->where('id_anggota',$id_anggota)->get();
    return view('pembimbing.editanggota', compact('anggota'));
}
//membuat fungsi untuk proses update
public function updatepem(Request $request, Anggota $anggota)
{
    DB::table('anggota')->where('id_anggota',$request->id_anggota)->update([
        'nama_ang' => $request->nama_ang,
        'jabatan' => $request->jabatan,
        'alamat' => $request->alamat,
        'semester' => $request->semester,
        'jurusan' => $request->jurusan,
        'kelas' => $request->kelas,
        'no_hp' => $request->no_hp,
        'email' => $request->email,
        'status' => $request->status
    ]);

    // anggota::find($request->id);
    return redirect('pembimbing/anggota');
}

//membuat fungsi untuk memproses hapus
public function hapuspem($id_anggota)
{
    Anggota::where('id_anggota',$id_anggota)->delete();
    return back()->with('deleted','Data Anggota Berhasil Terhapus!!!');
}

// -----------Akhir - Pengajuan - Ketua--------------------------------------------------

}
