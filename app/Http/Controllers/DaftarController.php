<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Daftar;
use Illuminate\Support\Facades\DB;

class DaftarController extends Controller
{
// -----------Awal  - Kelola - Pendaftar--------------------------------------------------

    //membuat fungsi untuk menampilkan data dari database ke dalam laravel
    public function index()
    {
        //mengambil data pendaftar dengan fungsi all (semua data)
    	$daftar = Daftar::all();
        // $pengajuan = Pengajuan::where('status','diterima')->get();

        //menampilkan data ke view ketua/pendaftar 
    	return view('ketua.daftar', compact('daftar'));
    }

    //membuat fungsi untuk beralih ke halaman form tambah pendaftar
    public function tambah()
    {
        //menampilkan halaman form addanggota
        return view('ketua.adddaftar');
    }

    //fungsi untuk membuat proses inputan data
    public function store(Request $request)
    {
        //validasi untuk mengisi kolom
        $this->validate($request,[
            'id_daftar' => 'required',
            'nama_daf' => 'required',
            'alamat' => 'required',
            'kelas' => 'required',
            'jurusan' => 'required',
            'motivasi' => 'required',
            'email' => 'required',
            'status' => 'required',
            'file' => 'required'
        ]);
        //fungsi untuk proses inputan data ke database
        Daftar::create([
            'id_daftar' => $request->id_daftar,
            'nama_daf' => $request->nama_daf,
            'alamat' => $request->alamat,
            'kelas' => $request->kelas,
            'jurusan' => $request->jurusan,
            'motivasi' => $request->motivasi,
            'email' => $request->email,
            'status' => $request->status,
            'file' => $request->file,
        ]);
        return redirect('ketua/daftar');
    }

    //fungsi untuk menampilkan form edit
    public function edit($id_daftar)
    {
        $daftar = DB::table('daftar')->where('id_daftar',$id_daftar)->get();
        return view('ketua.editdaftar', compact('daftar'));
    }
    //membuat fungsi untuk proses update
    public function update(Request $request, Daftar $daftar)
    {
        DB::table('daftar')->where('id_daftar',$request->id_daftar)->update([
            'nama_daf' => $request->nama_daf,
            'alamat' => $request->alamat,
            'kelas' => $request->kelas,
            'jurusan' => $request->jurusan,
            'motivasi' => $request->motivasi,
            'email' => $request->email,
            'status' => $request->status,
            'file' => $request->file
        ]);

        // anggota::find($request->id);
        return redirect('ketua/daftar');
    }

    //membuat fungsi untuk memproses hapus
    public function hapus($id_daftar)
    {
        Daftar::where('id_daftar',$id_daftar)->delete();
        return back()->with('deleted','Data Pendaftar Berhasil Terhapus!!!');
    }

// -----------Akhir - Pendaftar - Ketua--------------------------------------------------


// -----------Awal  - Kelola - Pendaftar - Pembimbing--------------------------------------------------

    //membuat fungsi untuk menampilkan data dari database ke dalam laravel
    public function indexpem()
    {
        //mengambil data pendaftar dengan fungsi all (semua data)
    	$daftar = Daftar::all();
        // $pengajuan = Pengajuan::where('status','diterima')->get();

        //menampilkan data ke view ketua/pendaftar 
    	return view('pembimbing.daftar', compact('daftar'));
    }

    //membuat fungsi untuk beralih ke halaman form tambah pendaftar
    public function tambahpem()
    {
        //menampilkan halaman form addanggota
        return view('pembimbing.adddaftar');
    }

    //fungsi untuk membuat proses inputan data
    public function storepem(Request $request)
    {
        //validasi untuk mengisi kolom
        $this->validate($request,[
            'id_daftar' => 'required',
            'nama_daf' => 'required',
            'alamat' => 'required',
            'kelas' => 'required',
            'jurusan' => 'required',
            'motivasi' => 'required',
            'email' => 'required',
            'status' => 'required',
            'file' => 'required'
        ]);
        //fungsi untuk proses inputan data ke database
        Daftar::create([
            'id_daftar' => $request->id_daftar,
            'nama_daf' => $request->nama_daf,
            'alamat' => $request->alamat,
            'kelas' => $request->kelas,
            'jurusan' => $request->jurusan,
            'motivasi' => $request->motivasi,
            'email' => $request->email,
            'status' => $request->status,
            'file' => $request->file,
        ]);
        return redirect('pembimbing/daftar');
    }

    //fungsi untuk menampilkan form edit
    public function editpem($id_daftar)
    {
        $daftar = DB::table('daftar')->where('id_daftar',$id_daftar)->get();
        return view('pembimbing.editdaftar', compact('daftar'));
    }
    //membuat fungsi untuk proses update
    public function updatepem(Request $request, Daftar $daftar)
    {
        DB::table('daftar')->where('id_daftar',$request->id_daftar)->update([
            'nama_daf' => $request->nama_daf,
            'alamat' => $request->alamat,
            'kelas' => $request->kelas,
            'jurusan' => $request->jurusan,
            'motivasi' => $request->motivasi,
            'email' => $request->email,
            'status' => $request->status,
            'file' => $request->file
        ]);

        // anggota::find($request->id);
        return redirect('pembimbing/daftar');
    }

    //membuat fungsi untuk memproses hapus
    public function hapuspem($id_daftar)
    {
        Daftar::where('id_daftar',$id_daftar)->delete();
        return back()->with('deleted','Data Pendaftar Berhasil Terhapus!!!');
    }

// -----------Akhir - Pendaftar - Pembimbing--------------------------------------------------

}
