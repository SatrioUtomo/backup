<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pengajuan;
use Illuminate\Support\Facades\DB;

class PengajuanController extends Controller
{
// -----------Awal - Pengajuan - Anggota--------------------------------------------------

    //membuat fungsi untuk menampilkan data dari database ke dalam laravel
    public function index()
    {
        //mengambil data pengajuan dengan fungsi all (semua data)
    	$pengajuan = Pengajuan::all();
        // $pengajuan = Pengajuan::where('status','diterima')->get();

        //menampilkan data ke view anggota/pengajuan 
    	return view('anggota.pengajuan', compact('pengajuan'));
    }

    //membuat fungsi untuk beralir ke halaman form tambah pengajuan
    public function tambah()
    {
        //menampilkan halaman form addpengajuan
        $user = DB::table('user')->selectRaw("id, name")->get();
        $data['user'] = $user;

        return view('anggota.addpengajuan', $data);
    }

    //fungsi untuk membuat proses inputan data
    public function store(Request $request)
    {
        //validasi untuk mengisi kolom
        $this->validate($request,[
            'id_user' => 'required',
            'nama_proker' => 'required',
            'detail' => 'required',
            'status' => 'required',
            'keterangan' => 'required'
        ]);
        //fungsi untuk proses inputan data ke database
        Pengajuan::create([
            'id' => $request->id,
            'id_user' => $request->id_user,
            'nama_proker' => $request->nama_proker,
            'detail' => $request->detail,
            'status' => $request->status,
            'keterangan' => $request->keterangan,
        ]);
        return redirect('anggota/pengajuan');
    }

    //fungsi untuk menampilkan form edit
    public function edit($id)
    {
        $pengajuan = DB::table('pengajuan')->where('id',$id)->get();
        $user = DB::table('user')->selectRaw("id, name")->get();
        return view('anggota.editpengajuan', compact('pengajuan','user'));
    }
    //membuat fungsi untuk proses update
    public function update(Request $request, Pengajuan $pengajuan)
    {
        DB::table('pengajuan')->where('id',$request->id)->update([
            'id_user' => $request->id_user,
            'nama_proker' => $request->nama_proker,
            'detail' => $request->detail,
            'status' => $request->status,
            'keterangan' => $request->keterangan
        ]);

        // Pengajuan::find($request->id);
        return redirect('anggota/pengajuan');
    }

    //membuat fungsi untuk memproses hapus
    public function hapus($id)
    {
        Pengajuan::where('id',$id)->delete();
        return back()->with('deleted','berhasil terhapus!!!');
    }

// -----------Akhir - Pengajuan - Anggota--------------------------------------------------


// -----------Awal - Pengajuan - Ketua--------------------------------------------------
    public function indexket()
    {
    //mengambil data pengajuan dengan fungsi all (semua data)
    
    $pengajuan = Pengajuan::join('user as u','u.id','=','pengajuan.id_user')
    ->selectRaw("pengajuan.*, u.id as user_id,name")
    ->get();
    // return $pengajuan;
    // $pengajuan = Pengajuan::where('status','diterima')->get();

    //menampilkan data ke view anggota/pengajuan 
    return view('ketua.pengajuan', compact('pengajuan'));
    }

        //fungsi untuk menampilkan form edit
    public function editket($id)
    {
        $pengajuan = DB::table('pengajuan')->where('id',$id)->get();
        $user = DB::table('user')->selectRaw("id, name")->get();
        return view('ketua.editpengajuan', compact('pengajuan','user'));
    }
    //membuat fungsi untuk proses update
    public function updateket(Request $request, Pengajuan $pengajuan)
    {
        DB::table('pengajuan')->where('id',$request->id)->update([
            'id_user' => $request->id_user,
            'nama_proker' => $request->nama_proker,
            'detail' => $request->detail,
            'status' => $request->status,
            'keterangan' => $request->keterangan
        ]);
        return redirect('ketua/pengajuan');
    }

    //membuat fungsi untuk memproses hapus
    public function hapusket($id)
    {
        Pengajuan::where('id',$id)->delete();
        return back()->with('deletedpengajuan','Data Pengajuan Berhasil Terhapus!!!');
    }

// -----------Akhir - Pengajuan - Ketua--------------------------------------------------
}
