<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function proses_login(Request $request)
    {

        $this->validate($request, [
            'username' => 'required|min:2',
            'password' => 'required|min:6'
        ]);

        $username = $request->input("username");
        $password = $request->input("password");
        // return 'ok';

        $data_user = DB::table('user')->where([['username' ,'=', $username]])->get();

        // dd($data_user);

        if (!isset($data_user[0]->username)) {
            $out = [
                "message" => "login_vailed",
                "code"    => 401,
                "result"  => [
                    "token" => null,
                ]
            ];
            return back()->with('salah','username tidak terdaftar !!!');
        }

        if (isset($data_user[0])) {
            
            if (Hash::check($password, $data_user[0]->password)) {
                // return $data_user[0]->level;
                if ($data_user[0]->level == 'ketua') {
                    return redirect('ketua');
                }elseif($data_user[0]->level == 'anggota') {
                    return redirect('anggota');
                } elseif ($data_user[0]->level == 'pembimbing') {
                    return redirect('pembimbing');
                }else{

                }
                return 'ok';
            }else{
                return back()->with('salah','password anda salah !!!');
            }
        }else{
            return back()->with('salah','ada kesalahan server, coba ulangi sekali lagi !!!');
        }
    }

    function upd_pass()
    {
        $query = DB::select("select * from user");
        // return $query;

        foreach ($query as $key => $login) {
                # code...
                $password = hash::make($login->password);
                $data=array(
                    'password'=>$password
                );
                DB::table('user')->where('id',"$login->id")->update($data);
        }
        return 'sukses';
    }
}
