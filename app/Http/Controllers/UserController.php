<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Datauser;
use Illuminate\Support\Facades\DB;
class UserController extends Controller
{
   //membuat fungsi untuk menampilkan data dari database ke dalam laravel
   public function index()
   {
       //mengambil data pengajuan dengan fungsi all (semua data)
       $user = Datauser::all();
      //  $pengajuan = Pengajuan::where('status','diterima')->get();

       //menampilkan data ke view anggota/pengajuan 
       return view('ketua.datauser', compact('user'));
   }
}
