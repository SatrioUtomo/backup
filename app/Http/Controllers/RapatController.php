<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rapat;
use Illuminate\Support\Facades\DB;

class RapatController extends Controller
{
    // ###################### RAPAT KETUA ########################################
     //membuat fungsi untuk menampilkan data dari database ke dalam laravel
     public function index()
     {
         //mengambil data pengajuan dengan fungsi all (semua data)
         $rapat = Rapat::all();
        //  $pengajuan = Pengajuan::where('status','diterima')->get();
 
         //menampilkan data ke view anggota/pengajuan 
         return view('ketua.rapat', compact('rapat'));
     }





     public function hapus($id)
     {
         Rapat::where('id',$id)->delete();
         return back()->with('deleted','berhasil terhapus!!!');
     }
}
