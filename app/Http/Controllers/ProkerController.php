<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Proker;
use Illuminate\Support\Facades\DB;

class ProkerController extends Controller
{
    //  -----------Awal - PROKER - KETUA--------------------------------------------------
    //membuat fungsi untuk menampilkan data dari database ke dalam laravel
    public function index()
    {
        //mengambil data proker dengan fungsi all (semua data)
        $proker = Proker::join('pengajuan as p','p.id','=','proker.id_pengajuan')
        ->selectRaw("proker.*, p.id as pengajuan_id,nama_proker,detail")
        // ->toSql();
        ->get();
        // return $proker;

        //menampilkan data ke view ketua/proker 
    	return view('ketua.proker', compact('proker'));
    }

      //membuat fungsi untuk beralir ke halaman form tambah proker
      public function tambah()
      {
          //menampilkan halaman form addproker
          $pengajuan = DB::table('pengajuan')
          ->whereRaw("pengajuan.deleted_at is null and pengajuan.keterangan='diterima'")
          ->selectRaw("id, nama_proker")
          ->get();
          $data['pengajuan'] = $pengajuan;

          
          return view('ketua.addproker', $data);
      }

        //fungsi untuk membuat proses inputan data
    public function store(Request $request)
    {
        //validasi untuk mengisi kolom
        $this->validate($request,[
            'id_pengajuan' => 'required',
            'tanggal' => 'required',
            'tempat' => 'required',
            'status' => 'required',
            'anggaran' => 'required'
        ]);
        //fungsi untuk proses inputan data ke database
        Proker::create([
            'id' => $request->id,
            'id_pengajuan' => $request->id_pengajuan,
            'tanggal' => $request->tanggal,
            'tempat' => $request->tempat,
            'status' => $request->status,
            'anggaran' => $request->anggaran,
        ]);
        return redirect('ketua/proker');
    }
        //fungsi untuk menampilkan form edit
        public function edit($id)
        {
            $proker = DB::table('proker')->where('id',$id)->get();
            $pengajuan = DB::table('pengajuan')
            ->whereRaw("pengajuan.deleted_at is null")
            ->selectRaw("id, nama_proker")
            ->get();
            return view('ketua.editproker', compact('proker','pengajuan'));
        }
        //membuat fungsi untuk proses update
        public function update(Request $request, Proker $proker)
        {
            DB::table('proker')->where('id',$request->id)->update([
            'id_pengajuan' => $request->id_pengajuan,
            'tanggal' => $request->tanggal,
            'tempat' => $request->tempat,
            'status' => $request->status,
            'anggaran' => $request->anggaran
            ]);
    
            // Pengajuan::find($request->id);
            return redirect('ketua/proker');
        }
    
        //membuat fungsi untuk memproses hapus
        public function hapus($id)
        {
            Proker::where('id',$id)->delete();
            return back()->with('deleted','berhasil terhapus!!!');
        }
}
